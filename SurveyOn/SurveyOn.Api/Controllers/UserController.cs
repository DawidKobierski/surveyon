﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using SurveyOn.Api.Filters;
using SurveyOn.Business.Interfaces;
using SurveyOn.Dto;

namespace SurveyOn.Api.Controllers
{
    [RoutePrefix("api/users")]
    public class UserController : ApiControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet, Route("")]
        [ResponseType(typeof(UserDto))]
        public IHttpActionResult GetUser(string email)
        {
            var user = _userService.GetUser(email);

            if (user != null)
            {
                return Ok(user);
            }
            return NotFound();
        }

        [HttpPost, Route("register")]
        [ValidateModel]
        public async Task<IHttpActionResult> Register(UserDto user)
        {
            var result = await _userService.RegisterUser(user);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}