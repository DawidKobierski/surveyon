﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using SurveyOn.Api.Filters;
using SurveyOn.Business.Interfaces;
using SurveyOn.Dto;

namespace SurveyOn.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/surveys/{surveyId}/sections")]
    public class SectionController : ApiControllerBase
    {
        private readonly ISectionService _sectionService;

        public SectionController(ISectionService sectionService)
        {
            _sectionService = sectionService;
        }

        [HttpGet, Route("")]
        [ResponseType(typeof(IEnumerable<SectionDto>))]
        public IHttpActionResult GetSections(int surveyId)
        {
            var sections = _sectionService.ReadSections(surveyId, UserId);

            if (sections != null)
            {
                return Ok(sections);
            }

            return NotFound();
        }

        [HttpGet, Route("{sectionId}", Name = "GetSection")]
        [ResponseType(typeof(SectionDto))]
        public IHttpActionResult GetSection(int sectionId)
        {
            var sections = _sectionService.ReadSection(sectionId, UserId);

            if (sections != null)
            {
                return Ok(sections);
            }

            return NotFound();
        }

        [HttpPost, Route("")]
        [ValidateModel]
        [ResponseType(typeof(QuestionDto))]
        public IHttpActionResult PostSection(int surveyId)
        {
            var section = _sectionService.CreateSection(surveyId);
            return CreatedAtRoute("GetSection", new { sectionId = section.Id }, section);
        }

        [HttpPatch, Route("{sectionId}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PatchSection(SectionDto section)
        {
            var resopnse = _sectionService.UpdateSection(section, UserId);

            if (resopnse)
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpPost, Route("{sectionId}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteSection(int sectionId)
        {
            var response = _sectionService.DeleteSection(sectionId, UserId);

            if (response)
            {
                return Ok();
            }

            return NotFound();
        }
    }
}
