﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using SurveyOn.Api.Filters;
using SurveyOn.Business.Interfaces;
using SurveyOn.Dto;

namespace SurveyOn.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/surveys/{surveyId}/sections/{sectionId}/questions")]
    public class QuestionController : ApiControllerBase
    {
        private readonly IQuestionService _questionService;

        public QuestionController(IQuestionService questionService)
        {
            _questionService = questionService;
        }

        [HttpGet, Route("{questionId}", Name = "GetQuestion")]
        [ResponseType(typeof(QuestionDto))]
        public IHttpActionResult GetQuestion(int questionId)
        {
            var question = _questionService.ReadQuestion(questionId, UserId);

            if (question != null)
            {
                return Ok(question);
            }

            return NotFound();
        }

        [HttpPost, Route("")]
        [ValidateModel]
        [ResponseType(typeof(QuestionDto))]
        public IHttpActionResult PostQuestion([FromBody]QuestionDto question, int sectionId)
        {
            var newQuestion = _questionService.CreateQuestion(question, sectionId);
            return CreatedAtRoute("GetQuestion", new { questionId = newQuestion.Id }, newQuestion);
        }

        [HttpPatch, Route("{questionId}")]
        [ValidateModel]
        [ResponseType(typeof(void))]
        public IHttpActionResult PatchQuestion(int questionId, [FromBody]QuestionDto question)
        {
            var response = _questionService.UpdateQuestion(question, UserId);

            if (!response)
                return NotFound();

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost, Route("{questionId}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteQuestion(int questionId)
        {
            if (!_questionService.DeleteQuestion(questionId, UserId))
                return NotFound();

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
