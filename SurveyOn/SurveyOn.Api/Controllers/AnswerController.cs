﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using SurveyOn.Api.Filters;
using SurveyOn.Business.Interfaces;
using SurveyOn.Dto;

namespace SurveyOn.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/surveys/{surveyId}/sections/{sectionId}/questions/{questionId}/answers")]
    public class AnswerController : ApiControllerBase
    {
        private readonly IAnswerService _answerService;

        public AnswerController(IAnswerService answerService)
        {
            _answerService = answerService;
        }

        [HttpGet, Route("")]
        [ResponseType(typeof(ReviewAnswersDto))]
        public IHttpActionResult GetAnswers(int surveyId)
        {
            var answers = _answerService.ReadAnswers(surveyId, UserId);

            if (answers != null)
            {
                return Ok(answers);
            }

            return NotFound();
        }

        [HttpGet, Route("{answerId}")]
        [ResponseType(typeof(IEnumerable<AnswerDto>))]
        public IHttpActionResult GetAnswer(int answerId)
        {
            var answers = _answerService.ReadAnswer(answerId, UserId);

            if (answers != null)
            {
                return Ok(answers);
            }

            return NotFound();
        }
    }
}
