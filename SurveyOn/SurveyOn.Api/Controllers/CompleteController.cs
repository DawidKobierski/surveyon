﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SurveyOn.Api.Filters;
using SurveyOn.Business.Interfaces;
using SurveyOn.Dto;

namespace SurveyOn.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/surveys/{surveyId}/answers")]
    public class CompleteController : ApiControllerBase
    {
        
            private readonly IAnswerService _answerService;

            public CompleteController(IAnswerService answerService)
            {
                _answerService = answerService;
            }

            [AllowAnonymous]
            [HttpPost, Route("")]
            [ValidateModel]
            [ResponseType(typeof(void))]
            public IHttpActionResult PostAnswer([FromBody] IEnumerable<AnswerDto> answers)
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                string address = null;
                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        address = addresses[0];
                    }
                }
                if (address == null)
                {
                    address = context.Request.ServerVariables["REMOTE_ADDR"];
                }
                foreach (var answer in answers)
                {
                    _answerService.CreateAnswer(answer, address);
                }
                return StatusCode(HttpStatusCode.NoContent);
            }
        }
}
