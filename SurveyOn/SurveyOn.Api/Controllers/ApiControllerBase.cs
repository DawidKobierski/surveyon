﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;

namespace SurveyOn.Api.Controllers
{
    public class ApiControllerBase : ApiController
    {
        public IEnumerable<Claim> Claims
        {
            get
            {
                var identity = (ClaimsPrincipal) Thread.CurrentPrincipal;
                return identity.Claims;
            }
        }

        public int UserId
        {
            get
            {
                return int.Parse(Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            }
        }
    }
}
