﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using SurveyOn.Api.Filters;
using SurveyOn.Business.Interfaces;
using SurveyOn.Dto;

namespace SurveyOn.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/surveys")]
    public class SurveyController : ApiControllerBase
    {
        private readonly ISurveyService _surveyService;

        public SurveyController(ISurveyService surveyService)
        {
            _surveyService = surveyService;
        }

        [HttpGet, Route("details")]
        [ResponseType(typeof(IEnumerable<DetailedSurveyDto>))]
        public IHttpActionResult GetDetailedSurveys()
        {
            var surveys = _surveyService.ReadDetailedSurveys(UserId);
            return Ok(surveys);
        }

        [HttpGet, Route("{surveyId}", Name = "GetSurvey")]
        [ResponseType(typeof(SurveyDto))]
        public IHttpActionResult GetSurvey(int surveyId)
        {
            var survey =  _surveyService.ReadSurvey(surveyId, UserId);          

            if (survey != null)
            {
                return Ok(survey);
            }
            return NotFound();
        }

        [AllowAnonymous]
        [HttpGet, Route("{surveyId}/complete")]
        [ResponseType(typeof(SurveyDto))]
        public IHttpActionResult GetSurveyForComplete(int surveyId)
        {
            var survey =  _surveyService.ReadSurvey(surveyId);

            if (survey != null)
            {
                return Ok(survey);
            }
            return NotFound();
        }

        [HttpGet, Route("")]
        [ResponseType(typeof(IEnumerable<SurveyDto>))]
        public IHttpActionResult GetSurveys()
        {
            var surveys = _surveyService.ReadSurveys(UserId);

            if (surveys != null)
            {
                return Ok(surveys);
            }

            return NotFound();
        }

        [HttpPost, Route("")]
        [ValidateModel]
        [ResponseType(typeof(SectionDto))]
        public IHttpActionResult PostSurvey()
        {
            var survey = _surveyService.CreateSurvey(UserId);
            return CreatedAtRoute("GetSurvey", new { surveyId = survey.Id }, survey);
        }

        [HttpPatch, Route("{surveyId}")]
        public IHttpActionResult PatchSurvey(SurveyDto survey)
        {
            bool response = _surveyService.UpdateSurvey(survey, UserId);

            if (response)
            {
                return Ok();
            }

            return NotFound();
        }
    }
}