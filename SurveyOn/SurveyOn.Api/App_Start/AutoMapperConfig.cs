﻿using AutoMapper;

namespace SurveyOn.Api
{
    public static class AutoMapperConfig
    {

        public static void RegisterProfiles()
        {
            Mapper.Initialize(cfg =>
                cfg.AddProfiles(new[] {
                "SurveyOn.Business"
                })
            );
        }
    }
}
