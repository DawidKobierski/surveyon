﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SurveyOn.Dto;
using SurveyOn.Front.Filters;

namespace SurveyOn.Front.Controllers
{
    public class SurveyController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        [Route("login")]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost, Route("authenticate")]
        public HttpStatusCodeResult Authenticate(UserDto dto)
        { 
            var authTicket = new FormsAuthenticationTicket(
                1,
                dto.Email,
                DateTime.Now,
                DateTime.Now.AddHours(24),
                false,
                ""
            );

            var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            HttpContext.Response.Cookies.Add(authCookie);

            return new HttpStatusCodeResult(HttpStatusCode.NoContent);
        }
        [SimpleAuthorization]
        [Route("edit/{surveyId}")]
        public ActionResult Edit(int surveyId)
        {
            return View();
        }
        
        [SimpleAuthorization]
        [Route("home")]
        public ActionResult Home()
        {
            return View();
        }

        [Route("test")]
        public ActionResult tempQuestionTypes()
        {
            return View();
        }

        [Route("register")]
        public ActionResult Register()
        {
            return View();
        }

        [Route("unauthorized", Name = "Unauthorized")]
        public ActionResult Unauthorized()
        {
            return View();
        }

        [Route("complete/{surveyId}")]
        public ActionResult Complete(int surveyId)
        {
            return View();
        }

        [Route("review/{surveyId}")]
        public ActionResult Review(int surveyId)
        {
            return View();
        }

        [Route("confirmation")]
        public ActionResult AnswerConfirmation()
        {
            return View();
        }
    }
}
