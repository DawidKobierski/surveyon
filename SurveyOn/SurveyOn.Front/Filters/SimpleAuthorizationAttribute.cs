﻿using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using SurveyOn.Business.Interfaces;
using SurveyOn.Business.Services;

namespace SurveyOn.Front.Filters
{
    public class SimpleAuthorizationAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var cookie = filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (cookie != null)
            {
                var ticketInfo = FormsAuthentication.Decrypt(cookie.Value);

                if (ticketInfo != null && !ticketInfo.Expired)
                    return;
            }

            filterContext.Result = new RedirectToRouteResult("Unauthorized", null);
        }
    }
}