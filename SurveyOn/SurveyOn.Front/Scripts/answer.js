﻿function Answer(data) {
    var self = this;

    self.questionId = 0;
    self.answerText = '';

    self.initialize = function (data) {
        self.questionId = data.questionId;
        self.answerText = data.answerText;
    };

    if (data !== null) {
        self.initialize(data);
    }
}