﻿var surveyOn;
(function (surveyOn) {
    (function (self) {
        self.questionTypes = {
            short:  {id: 1, name: "short"},
            long: { id: 2, name: "long" }
        }
    }(surveyOn.enums = surveyOn.enums || {}));
}(surveyOn = surveyOn || {}));