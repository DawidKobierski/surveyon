﻿function Section(data) {
    var self = this;

    self.id = ko.observable();
    self.title = ko.observable();
    self.description = ko.observable();
    self.questions = ko.observableArray();

    self.initialize = function(section) {
        self.id(section.Id);
        self.title(section.Title);
        self.description(section.Description);
        self.questions($.map(section.Questions,function(question){
            return new QuestionViewModel(question, self.id());
            }));
    };

    if (data !== null) {
        self.initialize(data);
    }

    self.title.subscribeWithBeforeValue(function (oldTitle, newTitle) {
        if (newTitle === null || newTitle.match(/^\s*$/) !== null ) {
            self.title(oldTitle);
        } else {
            var updatedSection = new Section(null);
            updatedSection.title = newTitle;
            updatedSection.id = self.id();

            var uri = '/api/surveys/0/sections/' + updatedSection.id;
            surveyOn.updateHelper.request(updatedSection, uri);
        }
    });

    self.description.subscribeWithBeforeValue(function (oldDescription, newDescription) {
        var updatedSection = new Section(null);
        updatedSection.description = newDescription;
        updatedSection.id = self.id();

        var uri = '/api/surveys/0/sections/' + updatedSection.id;

        surveyOn.updateHelper.request(updatedSection, uri);
    });

    self.addQuestion = function() {
        surveyOn.ajaxHelper.request(
            '/api/surveys/0/sections/' + self.id() + '/questions',
            'POST',
            {QuestionType: { Name: "short" }, Required: true },
            function(response) {
                self.questions.push(new QuestionViewModel(response));
                surveyOn.utilityApplier.apply.expandableTextareas();
            });
    };

    self.removeQuestion = function (item) {
        surveyOn.ajaxHelper.request(
            '/api/surveys/0/sections/0/questions/' + item.id(),
            'POST',
            null,
            function (response) {
            });
        self.questions.remove(item);
    }
}
