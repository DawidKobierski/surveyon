﻿function ReviewViewModel() {
    var self = this;

    self.surveyTitle = ko.observable();
    self.answers = ko.observableArray([]);
    self.surveyId = window.location.href.split('/').pop();

    surveyOn.ajaxHelper.request(
        '/api/surveys/' + self.surveyId + '/sections/0/questions/0/answers',
        'GET',
        null,
        function (response) {
            surveyOn.utilityApplier.apply.destroyDataTable();
            self.surveyTitle(response.SurveyTitle);
            self.answers($.map(response.Answers, function(item) {
                return new ReviewAnswerViewModel(item);
            }));
            surveyOn.utilityApplier.apply.updateDataTable(self.surveyTitle());
            surveyOn.utilityApplier.apply.tooltips();
        }
    );
}

ko.applyBindings(new ReviewViewModel());