﻿function QuestionViewModel(data) {
    var self = this;

    self.id = ko.observable();
    self.title = ko.observable();
    self.description = ko.observable();
    self.answer = ko.observable();
    self.required = ko.observable();

    self.questionType = ko.observable();
    self.questionTypes = ko.observableArray($.map(surveyOn.enums.questionTypes, function(qt) {
        return {
            name: qt.name,
            id: qt.id
        };
    }));

    self.initialize = function(question) {
        self.id(question.Id);
        self.title(question.Title);
        self.description(question.Description);
        self.questionType(question.QuestionType.Id);
        self.required(question.Required === "True" ? true : false);
    };

    if (data !== null) {
        self.initialize(data);
    }

    self.title.subscribeWithBeforeValue(function (oldTitle, newTitle) {
        if (newTitle === "" || newTitle.match(/^\s*$/) !== null) {
            self.title(oldTitle);
        } else {
            var updatedQuestion = new QuestionViewModel(null);
            updatedQuestion.title = newTitle;
            updatedQuestion.id = self.id();
            updatedQuestion.required = self.required();

            var uri = '/api/surveys/0/sections/0/questions/' + updatedQuestion.id;
            surveyOn.updateHelper.request(updatedQuestion, uri);
        }
    });

    self.required.subscribe(function (required) {
        var updatedQuestion = new QuestionViewModel(null);
        updatedQuestion.required = required;
        updatedQuestion.id = self.id();

        var uri = '/api/surveys/0/sections/0/questions/' + updatedQuestion.id;
        surveyOn.updateHelper.request(updatedQuestion, uri);
    });

    self.description.subscribeWithBeforeValue(function (oldDescription, newDescription) {
        var updatedQuestion = new QuestionViewModel(null);
        updatedQuestion.title = self.title();
        updatedQuestion.description = newDescription;
        updatedQuestion.id = self.id();
        updatedQuestion.required = self.required();

        var uri = '/api/surveys/0/sections/0/questions/' + updatedQuestion.id;
        surveyOn.updateHelper.request(updatedQuestion, uri);
    });

    self.updateQuestionType = function (data) {
        var updatedQuestion = new QuestionViewModel(null);
        updatedQuestion.title = self.title();
        updatedQuestion.questionType = { name: "", id: data.questionType()};
        updatedQuestion.required = data.required();
        updatedQuestion.id = data.id();

        var uri = '/api/surveys/0/sections/0/questions/' + updatedQuestion.id;
        surveyOn.updateHelper.request(updatedQuestion, uri);
        surveyOn.utilityApplier.apply.expandableTextareas();
    };

    ko.bindingHandlers.isrequired = {
        init: function(element, valueAccessor) {
            var a = ko.unwrap(valueAccessor)();
            if (a()) {
                element.required = true;
            }
        }
    };
}