﻿function LoginViewModel() {
    var self = this;
    self.emailAddress = ko.observable();
    self.password = ko.observable();

    self.login = function () {
        $.ajax({
            url: window.location.protocol + '//' + window.location.hostname + ':6378' + '/token',
            method: 'POST',
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
            data: {
                grant_type: 'password',
                userName: self.emailAddress(),
                password: self.password()
            }
        })
            .done(function (response) {
                sessionStorage.setItem('token', response.access_token);
                sessionStorage.setItem('email', self.emailAddress());
                $.ajax({
                        url: window.location.protocol + '//' + window.location.host + '/authenticate',
                        method: 'POST',
                        data: { Email: self.emailAddress() },
                        dataType: 'json'
                    })
                    .done(function() {
                        window.location.replace('/home');
                    });
            })
            .fail(function () {
                surveyOn.utilityApplier.apply.popupError();
            });
    };
}

ko.applyBindings(new LoginViewModel());