﻿function RegisterViewModel() {
    var self = this;
    self.email = ko.observable("");
    self.password = ko.observable("");
    self.confirmPassword = ko.observable("");
    self.agreeToTerms = ko.observable(false);

    self.checkPassword = function (input) {
        var regxp = /^([a-zA-Z0-9_-]){6,24}$/;

        if (regxp.test(self.password()) === false) {
            input.setCustomValidity('Password must have between 6 and 24 characters');
        } else {
            input.setCustomValidity("");
        }
    };

    self.checkConfirmPassword = function (input) {

        if (self.password() !== self.confirmPassword()) {
            input.setCustomValidity('Passwords do not match!');
        } else {
            input.setCustomValidity("");
        }
    };

    self.checkAgreeToTerms = function (checkbox) {

        if (!self.agreeToTerms()) {
            checkbox.setCustomValidity('You have to agree to terms.');
        } else {
            checkbox.setCustomValidity("");
        }
    };

    self.registerUser = function () {
        surveyOn.ajaxHelper.request(
            registerUri,
            'POST',
            ko.toJS(self),
            function() {
                window.location.href = "/login";
            },
            function () {
                surveyOn.utilityApplier.apply.popupError();
            }
        );
    };
}

var registerUri = '/api/users/register';

$(document)
    .ready(function () {
        window.registerModel = new RegisterViewModel();

        ko.applyBindings(registerModel);
    });