﻿var surveyOn;
(function(surveyOn) {
    (function(self, $) {
        self.request = function(updatedData, uri) {
            surveyOn.ajaxHelper.request(
                uri,
                'PATCH',
                updatedData,
                function(response) {
                }
            );
        };
    }(surveyOn.updateHelper = surveyOn.updateHelper || {}, jQuery));
}(surveyOn = surveyOn || {}));