﻿var surveyOn;
(function (surveyOn) {
    (function (self, $) {
        self.request = function (uri, method, data, successCallback, failCallback) {
            var ajaxRequest = $.ajax({
                type: method,
                beforeSend: function (request) {
                    var token = sessionStorage.getItem('token');
                    if (token) {
                        request.setRequestHeader("Authorization", 'Bearer ' + token);
                    }
                },
                url: window.location.protocol +
                    '//' +
                    window.location.hostname +
                    ":6378" +
                    uri,
                contentType: 'application/json;charset=UTF-8',
                data: data ? JSON.stringify(data) : null
            })
                .fail(function (jqXhr, textStatus, errorThrown) {
                    if (failCallback) {
                        failCallback(jqXhr, textStatus, errorThrown);
                    }
                    console.log(textStatus);
                })
                .done(function (response) {
                    if (successCallback) {
                        successCallback(response);
                    }
                });
            return ajaxRequest;
        };
    }(surveyOn.ajaxHelper = surveyOn.ajaxHelper || {}, jQuery));
}(surveyOn = surveyOn || {}));