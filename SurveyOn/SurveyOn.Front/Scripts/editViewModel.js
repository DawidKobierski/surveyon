﻿function EditViewModel() {
    var self = this;

    var surveyUri = '/api/surveys/';

    self.survey = ko.observable();
    self.survey.id = window.location.href.split("/").pop();

    self.numberOfQuestionsInRemovingSection = ko.observable();
    var removingSectionId;

    self.sectionSettingsPopoverContent = ko.observable('section-settings-popover-content');
    self.surveySettingsPopoverContent = ko.observable('survey-settings-popover-content');

    self.publishedSurveyUrl = ko.observable();


    self.removeSectionConfirmation = function (section) {

        self.numberOfQuestionsInRemovingSection(section.questions().length);
        removingSectionId = section.id();

        if (self.numberOfQuestionsInRemovingSection() === 0) {
            self.removeSection();
        } else {
            surveyOn.utilityApplier.apply.showModal('#confirm-delete-section-modal', 'show');
        }
    }

    self.removeSection = function () {
        self.survey().removeSection(removingSectionId);
        surveyOn.utilityApplier.apply.showModal('#confirm-delete-section-modal', 'hide');
    }

    self.publishSurvey = function (isPublished) {
        if (isPublished) {
            surveyOn.utilityApplier.apply.showModal('#confirm-publish-survey-modal', 'show');
            surveyOn.utilityApplier.apply.select("#published-modal-complete-input");
        }
    }

    self.copyCompliteUrlToClipboard = function () {
        surveyOn.utilityApplier.apply.select("#published-modal-complete-input");
        document.execCommand("copy");
    }

    self.goToPublishedSurvey = function () {
        window.location.href = self.publishedSurveyUrl();
    }

    surveyOn.ajaxHelper.request(
        surveyUri + self.survey.id,
        'GET',
        null,
        function (response) {
            self.survey(new Survey(response));
            surveyOn.utilityApplier.apply.expandableTextareas();
            surveyOn.utilityApplier.apply.tooltips();
            self.survey().isPublished.subscribe(function (isPublished) {
                self.publishSurvey(isPublished);
            });
            self.publishedSurveyUrl(window.location.href.replace("edit", "complete"));
        }
    );
}

$(document)
    .ready(function () {
        ko.applyBindings(new EditViewModel());
    });
