﻿ko.subscribable.fn.subscribeWithBeforeValue = function (callback) {
    var _oldValue;
    this.subscribe(function (oldValue) {
        _oldValue = oldValue;
    }, null, 'beforeChange');

    var subscription = this.subscribe(function (newValue) {
        callback.call(callback, _oldValue, newValue);
    });

    return subscription;
};