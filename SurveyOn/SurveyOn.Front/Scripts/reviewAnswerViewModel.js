﻿function ReviewAnswerViewModel(data) {
    var self = this;

    self.answer = ko.observable();

    self.sectionId = ko.observable();
    self.sectionTitle = ko.observable();
    self.section = ko.pureComputed(function () {
        return self.sectionId() + '. ' + self.sectionTitle();
    });

    self.questionId = ko.observable();
    self.questionTitle = ko.observable();
    self.question = ko.pureComputed(function () {
        return self.questionId() + '. ' + self.questionTitle();
    });

    self.initialize = function(reviewAnswer) {
        self.answer(reviewAnswer.AnswerText);
        self.sectionId(reviewAnswer.SectionId);
        self.sectionTitle(reviewAnswer.SectionTitle);
        self.questionId(reviewAnswer.QuestionId);
        self.questionTitle(reviewAnswer.QuestionTitle);
    }

    if (data !== null) {
        self.initialize(data);
    }
}