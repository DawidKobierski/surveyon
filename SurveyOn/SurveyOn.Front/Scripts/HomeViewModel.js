﻿var SurveyTile = function (item) {
    var self = this;
    self.Id = ko.observable();
    self.Title = ko.observable();
    self.Description = ko.observable();
    self.PublishedDate = ko.observable();
    self.NumberOfSections = ko.observable();
    self.NumberOfQuestions = ko.observable();
    self.NumberOfResponses = ko.observable();


    self.initialize = function (data) {
        self.Id(data.Id);
        self.Title(data.Title);
        self.Description(data.Description);
        self.PublishedDate(data.PublishedDate);
        self.NumberOfSections(data.NumberOfSections);
        self.NumberOfQuestions(data.NumberOfQuestions);
        self.NumberOfResponses(data.NumberOfResponses);
    };

    if (item !== null) {
        self.initialize(item);
    }

    self.goToSurvey = function () {
        var surveyUrl = window.location.origin + "/edit/" + this.Id();
        window.location.href = surveyUrl;
    };
};


function HomeViewModel() {
    var self = this;


    self.surveys = ko.observableArray([]);

    self.addSurvey = function (data) {
        self.surveys.push(new SurveyTile(data));
        self.updateMasonry();
    }

    self.updateMasonry = function () {
        var newSurvey = $('.grid-item:last');
        surveyOn.utilityApplier.apply.updateMasonry(newSurvey);
        surveyOn.utilityApplier.apply.tooltips();
    }

    var uri = "/api/surveys/details";
    surveyOn.ajaxHelper.request(
        uri,
        'GET',
        null,
        function (response) {
            $.each(response,
                function (key, value) {
                    var that = value;
                    self.addSurvey(
                    {
                        Id: that.Id,
                        Title: that.Title,
                        Description:
                           that.Description,
                        PublishedDate:
                           that.PublishedDate,
                        NumberOfQuestions:
                            that.NumberOfQuestions,
                        NumberOfResponses:
                            that.NumberOfResponses,
                        NumberOfSections:
                            that.NumberOfSections
                    });
                });
        }
    );

}


$(document)
    .ready(function () {
        ko.applyBindings(new HomeViewModel());
        surveyOn.utilityApplier.apply.masonry();
        surveyOn.utilityApplier.apply.tooltips();
    });
