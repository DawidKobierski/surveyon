﻿var surveyOn;
(function (surveyOn) {
    (function (self, $) {
        self.apply =
        {
            tooltips: function () {
                $('[data-toggle="tooltip"]').tooltip();
            },

            masonry: function () {
                $('.grid')
                    .masonry({
                        itemSelector: '.grid-item',
                        columnWidth: 280
                    });
            },

            updateMasonry: function (newSurvey) {
                $('.grid').masonry('appended', newSurvey);
            },

            expandableTextareas: function () {
                $(".expanding").expanding('refresh');
            },

            popupError: function () {
                $('.errPopup').show();
            },

            clearCookies: function () {
                document.cookie.split(";")
                    .forEach(function (c) {
                        document.cookie = c.replace(/^ +/, "")
                            .replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
                    });
            },

            showModal: function (modalId, option) {
                $(modalId).modal(option);
            },

            updateDataTable: function (surveyTitle) {
                var table = $('#answers');
                var cells = table.find('td');
                $.each(cells,
                    function (index, item) {
                        var content = $(item).text();
                        $(item).attr('title', content);
                    });
                table.DataTable({
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            title: surveyTitle
                        },
                        {
                            extend: 'excel',
                            title: surveyTitle
                        },
                        {
                            extend: 'pdf',
                            title: surveyTitle
                        },
                        {
                            extend: 'print',
                            title: surveyTitle
                        }
                    ]
                });
            },

            destroyDataTable: function() {
                var table = $('#answers').dataTable();
                table.fnDestroy();
            },

            select: function(id) {
                $(id).select();
            }
        }
    }(surveyOn.utilityApplier = surveyOn.utilityApplier || {}, jQuery));
}(surveyOn = surveyOn || {}));
