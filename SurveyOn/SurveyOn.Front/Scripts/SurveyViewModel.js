﻿var Survey = function (item) {
    var self = this;
    self.id = ko.observable();
    self.title = ko.observable();
    self.description = ko.observable();
    self.sections = ko.observableArray([]);
    self.isPublished = ko.observable();

    self.initialize = function (survey) {
        self.id(survey.Id);
        self.title(survey.Title);
        self.description(survey.Description);
        self.sections($.map(survey.Sections, function (section) {
            return new Section(section);
        }));
        self.isPublished(survey.IsPublished === "True");
    };

    if (item !== null) {
        self.initialize(item);
    }

    self.addSection = function () {
        surveyOn.ajaxHelper.request(
                        '/api/surveys/' + self.id() + '/sections',
                        'POST',
                        null,
                        function (response) {
                            self.sections.push(new Section(response));
                            surveyOn.utilityApplier.apply.expandableTextareas();
                        });
    };

    self.isPublished.subscribe(function(isPublished) {
        var updatedSurvey = new Survey(null);
        updatedSurvey.isPublished = isPublished;
        updatedSurvey.id = self.id();

        var uri = '/api/surveys/' + updatedSurvey.id;
        surveyOn.updateHelper.request(updatedSurvey, uri);
    });

    self.title.subscribeWithBeforeValue(function (oldTitle, newTitle) {
        if (newTitle === null || newTitle.match(/^\s*$/) !== null) {
            self.title(oldTitle);
        } else {
            var updatedSurvey = new Survey(null);
            updatedSurvey.title = newTitle;
            updatedSurvey.id = self.id();

            var uri = '/api/surveys/' + updatedSurvey.id;
            surveyOn.updateHelper.request(updatedSurvey, uri);
        }
    });

    self.description.subscribeWithBeforeValue(function (oldDescription, newDescription) {
        var updatedSurvey = new Survey(null);
        updatedSurvey.description = newDescription;
        updatedSurvey.id = self.id();

        var uri = '/api/surveys/' + updatedSurvey.id;
        surveyOn.updateHelper.request(updatedSurvey, uri);
    });

    self.removeSection = function (sectionId) {
        surveyOn.ajaxHelper.request(
            '/api/surveys/0/sections/' + sectionId,
            'POST',
            null,
            function (response) {
            });

        self.sections.remove(function (section) { return section.id() === sectionId; });
    }
};
