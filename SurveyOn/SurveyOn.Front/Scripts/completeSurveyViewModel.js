﻿function CompleteSurveyViewModel() {
    var self = this;

    self.survey = ko.observable();
    self.survey.id = window.location.href.split('/').pop();
    self.published = ko.observable(true);

    surveyOn.ajaxHelper.request(
        '/api/surveys/' + self.survey.id + '/complete',
        'GET',
        null,
        function(response) {
            self.survey(new Survey(response));
            surveyOn.utilityApplier.apply.expandableTextareas();
            surveyOn.utilityApplier.apply.tooltips();
        },
        function() {
            self.published(false);
        }
    );

    self.fillAnswersArray = function () {
        var answers = $.map(self.survey().sections(),
            function(section) {
                return $.map(section.questions(),
                    function(question) {
                        if (question.answer() != null) {
                            return {
                                answerText: question.answer(),
                                questionId: question.id()
                            };
                        }
                    });
            });
        if (answers.length) {
            self.sendAnswers(answers);
        }
    }
    self.sendAnswers = function(answers) {
        surveyOn.ajaxHelper.request(
            '/api/surveys/0/answers',
            'POST',
            answers,
            function () {
                window.location.href = "/confirmation";
            }
        );
    }
}

ko.applyBindings(new CompleteSurveyViewModel());