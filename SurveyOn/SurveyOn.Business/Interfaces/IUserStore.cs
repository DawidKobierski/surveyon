﻿using System.Threading.Tasks;
using SurveyOn.Data.Models;

namespace SurveyOn.Business.Interfaces
{
    public interface IUserStore
    {
        Task CreateAsync(User user);
        Task<User> FindByEmailAsync(string email);
    }
}