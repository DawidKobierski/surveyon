﻿using System.Collections.Generic;
using SurveyOn.Dto;

namespace SurveyOn.Business.Interfaces
{
    public interface ISurveyService
    {
        IEnumerable<SurveyDto> ReadSurveys(int userId);
        SurveyDto ReadSurvey(int surveyId, int userId = 0);
        IEnumerable<DetailedSurveyDto> ReadDetailedSurveys(int userId);
        SurveyDto CreateSurvey(int userId);
        bool UpdateSurvey(SurveyDto dto, int userId);
        bool DeleteSurvey(int surveyId, int userId);
    }
}
