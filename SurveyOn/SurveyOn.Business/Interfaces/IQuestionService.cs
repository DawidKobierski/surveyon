﻿using SurveyOn.Dto;

namespace SurveyOn.Business.Interfaces
{
    public interface IQuestionService
    {
        QuestionDto CreateQuestion(QuestionDto question, int sectionId);
        bool UpdateQuestion(QuestionDto question, int userId);
        bool DeleteQuestion(int questionId, int userId);
        QuestionDto ReadQuestion(int questionId, int userId);
    }
}