﻿using System.Collections.Generic;
using SurveyOn.Dto;

namespace SurveyOn.Business.Interfaces
{
    public interface IAnswerService
    {
        AnswerDto CreateAnswer(AnswerDto answer, string responseIp);
        ReviewAnswersDto ReadAnswers(int SurveyId, int userId);
        AnswerDto ReadAnswer(int answerId, int userId);
    }
}
