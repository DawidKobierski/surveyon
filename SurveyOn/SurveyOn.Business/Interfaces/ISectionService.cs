﻿using System.Collections.Generic;
using SurveyOn.Dto;

namespace SurveyOn.Business.Interfaces
{
    public interface ISectionService
    {
        IEnumerable<SectionDto> ReadSections(int surveyId, int userId);
        SectionDto CreateSection(int surveyId);
        bool UpdateSection(SectionDto section, int userId);
        bool DeleteSection(int sectionId, int userId);
        SectionDto ReadSection(int sectionId, int userId);
    }
}
