﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Interfaces
{
    public interface IUserService
    {
        UserDto GetUser(string email);
        Task<IdentityResult> RegisterUser(UserDto user);
        Task<User> FindUser(string email, string password);
    }
}
