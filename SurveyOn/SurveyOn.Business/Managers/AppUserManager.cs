﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SurveyOn.Business.Cryptography;
using SurveyOn.Data.Models;

namespace SurveyOn.Business.Managers
{
    public class AppUserManager
    {
        private readonly AppPasswordHasher _passwordHasher;
        private readonly AppUserStore _userStore;

        public AppUserManager()
        {
            _userStore = new AppUserStore();
            _passwordHasher = new AppPasswordHasher();
        }

        public async Task<IdentityResult> CreateAsync(string email, string password)
        {
            var dbEntry = await _userStore.FindByEmailAsync(email).ConfigureAwait(false);
            if (dbEntry == null)
            {
                var passwordHash = _passwordHasher.HashPassword(password);
                var user = new User
                {
                    Email = email,
                    PasswordHash = passwordHash
                };
                await _userStore.CreateAsync(user).ConfigureAwait(false);
                return IdentityResult.Success;
            }
            return IdentityResult.Failed("Account with this email already exists!");
        }

        public async Task<User> FindAsync(string email, string password)
        {
            var user = await _userStore.FindByEmailAsync(email).ConfigureAwait(false);
            if (user == null)
            {
                return null;
            }
            return _passwordHasher.VerifyHashedPassword(user.PasswordHash, password) ? user : null;
        }
    }
}