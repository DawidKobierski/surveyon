﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using SurveyOn.Business.Interfaces;
using SurveyOn.Data.Models;

namespace SurveyOn.Business.Managers
{
    public class AppUserStore : IUserStore
    {
        private readonly SurveyContext _db = new SurveyContext();

        public Task CreateAsync(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            _db.Users.Add(user);
            _db.SaveChangesAsync();
            return Task.FromResult(true);
        }

        public Task<User> FindByEmailAsync(string email)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }
            return _db.Users.SingleOrDefaultAsync(u => u.Email.Equals(email));
        }
    }
}