﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using AutoMapper;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Profiles
{
    public class SurveyProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Survey, SurveyDto>().ForMember(dest => dest.PublishedDate, o => o.ResolveUsing(src =>src.PublishDate.ToString()));

            CreateMap<Survey, DetailedSurveyDto>()
                .ForMember(dest => dest.NumberOfQuestions,
                    o => o.ResolveUsing(src => src.Sections.Sum(section => section.Questions.ToList().Count)))
                .ForMember(dest => dest.NumberOfSections, o => o.ResolveUsing(src => src.Sections.Count))
                .ForMember(dest => dest.NumberOfResponses, o => o.ResolveUsing(src => 
                (from section in src.Sections from question in section.Questions from answer in question.Answers select answer)
                .GroupBy(x => x.AnswererIp).Count()))
                .ForMember(dest => dest.PublishedDate, o => o.ResolveUsing(src => src.PublishDate.ToString()));

            CreateMap<SurveyDto, Survey>();
        }
    }
}
