﻿using AutoMapper;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Profiles
{
    public class QuestionProfile : Profile
    {
        protected override void Configure()
        {
            this.CreateMap<Question, QuestionDto>();
            this.CreateMap<QuestionDto, Question>().ForMember(dest => dest.Required, o => o.ResolveUsing(src => src.Required.ToString()));
        }
    }
}
