﻿using AutoMapper;
using SurveyOn.Business.Cryptography;
using SurveyOn.Data.Models;
using SurveyOn.Dto;


namespace SurveyOn.Business.Profiles
{
    public class UserProfile : Profile
    {

        protected override void Configure()
        {
                this.CreateMap<User, UserDto>().ForMember(dest => dest.EmailHash, o => o.MapFrom(src => Md5Hash.GetMd5Hash(src.Email.ToLower().Trim())));
        }

    }

}

