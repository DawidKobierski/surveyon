﻿using AutoMapper;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Profiles
{
    public class SectionProfile : Profile
    {
        protected override void Configure()
        {
            this.CreateMap<Section, SectionDto>();
            this.CreateMap<SectionDto, Section>();
        }
    }
}
