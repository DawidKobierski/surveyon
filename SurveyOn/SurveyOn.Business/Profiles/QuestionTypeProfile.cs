﻿using AutoMapper;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Profiles
{
    public class QuestionTypeProfile : Profile
    {
        protected override void Configure()
        {
            this.CreateMap<QuestionType, QuestionTypeDto>();
            this.CreateMap<QuestionTypeDto, QuestionType>();
        }
    }
}
