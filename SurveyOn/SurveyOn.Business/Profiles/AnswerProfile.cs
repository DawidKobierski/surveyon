﻿using AutoMapper;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Profiles
{
    public class AnswerProfile : Profile
    {
        protected override void Configure()
        {
            this.CreateMap<Answer, AnswerDto>();
            this.CreateMap<AnswerDto, Answer>();
        }
    }
}

