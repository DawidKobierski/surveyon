﻿using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using AutoMapper;
using SurveyOn.Business.Interfaces;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly SurveyContext _db = new SurveyContext();

        public QuestionDto CreateQuestion(QuestionDto question, int sectionId)
        {
            var temp = Mapper.Map<QuestionDto, Question>(question);
            temp.SectionId = sectionId;
            temp.QuestionType = _db.QuestionTypes.SingleOrDefault(x => x.Name == question.QuestionType.Name);
            _db.Questions.Add(temp);
            _db.SaveChanges();
            return Mapper.Map<Question, QuestionDto>(temp);
        }

        public QuestionDto ReadQuestion(int questionId, int userId)
        {
            var question = _db.Questions.SingleOrDefault(x => x.Id == questionId && x.Section.Survey.UserId == userId);

            if (question == null)
            {
                return null;
            }

            var questionDto = Mapper.Map<Question, QuestionDto>(question);

            return questionDto;
        }

        public bool UpdateQuestion(QuestionDto questionDto, int userId)
        {
            if (questionDto.QuestionType != null && questionDto.QuestionType.Name == "")
            {
                var updatedQuestionTypeName = _db.QuestionTypes.First(qt => qt.Id == questionDto.QuestionType.Id).Name;
                questionDto.QuestionType.Name = updatedQuestionTypeName;
            }

            var question = _db.Questions.SingleOrDefault(x => x.Id == questionDto.Id && x.Section.Survey.UserId == userId);

            if (question == null)
            {
                return false;
            }
            

            var questionUpdate = Mapper.Map<QuestionDto, Question>(questionDto);

            if (questionDto.QuestionType != null)
            {
                questionUpdate.QuestionTypeId = questionDto.QuestionType.Id; 
            }

            var questionProperties = typeof(Question).GetProperties();

            foreach (PropertyInfo prop in questionProperties)
            {
                if (prop.GetValue(questionUpdate) != null
                    && prop.Name != nameof(Question.Id)
                    && prop.Name != nameof(Question.SectionId)
                    && prop.Name != nameof(Question.Answers)
                    && prop.Name != nameof(Question.QuestionType))
                {
                    if (questionUpdate.QuestionTypeId == 0 && prop.Name == nameof(Question.QuestionTypeId))
                    {
                        break;
                    }
                    prop.SetValue(question, prop.GetValue(questionUpdate));
                }
            }


            _db.SaveChanges();

            return true;
            }

        public bool DeleteQuestion(int questionId, int userId)
        {
            var dbEntry = _db.Questions.SingleOrDefault(q => q.Id == questionId && q.Section.Survey.UserId == userId);
            if (dbEntry != null)
            {
                _db.Questions.Remove(dbEntry);
                _db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
