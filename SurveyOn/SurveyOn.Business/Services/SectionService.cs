﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using AutoMapper;
using SurveyOn.Business.Interfaces;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Services
{
    public class SectionService : ISectionService
    {
        private readonly SurveyContext _db = new SurveyContext();

        public IEnumerable<SectionDto> ReadSections(int surveyId, int userId)
        {
            var sections = _db.Sections
                .Where(x => x.SurveyId == surveyId && x.Survey.UserId == userId)
                .ToList()
                .Select(Mapper.Map<Section, SectionDto>);
            return sections;
        }

        public SectionDto ReadSection(int sectionId, int userId)
        {
            var section = _db.Sections.SingleOrDefault(x => x.Id == sectionId && x.Survey.UserId == userId);

            if (section == null)
            {
                return null;
            }

            var sectionDto = Mapper.Map<Section, SectionDto>(section);

            return sectionDto;
        }

        public SectionDto CreateSection(int surveyId)
        {
            var section = new Section {SurveyId = surveyId};
            _db.Sections.Add(section);
            _db.SaveChanges();
            return Mapper.Map<Section, SectionDto>(section);
        }

        public bool UpdateSection(SectionDto sectionDto, int userId)
        {
            var section = _db.Sections.SingleOrDefault(x => x.Id == sectionDto.Id && x.Survey.UserId == userId);
            if (section == null)
            {
                return false;
            }

            var sectionUpdate = Mapper.Map<SectionDto, Section>(sectionDto);
            var sectionProperties = typeof(Section).GetProperties();
            foreach (PropertyInfo prop in sectionProperties)
            {

                if (prop.GetValue(sectionUpdate) != null
                    && prop.Name != nameof(Section.Id)
                    && prop.Name != nameof(Section.SurveyId)
                    && prop.Name != nameof(Section.Questions))
                {
                    prop.SetValue(section, prop.GetValue(sectionUpdate));
                }
            }

            _db.SaveChanges();

            return true;
        }

        public bool DeleteSection(int sectionId, int userId)
        {
            var section = _db.Sections.SingleOrDefault(s => s.Id == sectionId && s.Survey.UserId == userId);   
            if (section != null)
            {
                _db.Sections.Remove(section);
                _db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
