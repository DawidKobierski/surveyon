﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SurveyOn.Business.Interfaces;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Services
{
    public class AnswerService: IAnswerService
    {
        private readonly SurveyContext _db = new SurveyContext();
        public AnswerDto CreateAnswer(AnswerDto answerDto, string responseIp)
        {
            var answer = Mapper.Map<AnswerDto, Answer>(answerDto);
            answer.AnswererIp = responseIp;
            _db.Answers.Add(answer);
            _db.SaveChanges();
            return Mapper.Map<Answer, AnswerDto>(answer);
        }

        public ReviewAnswersDto ReadAnswers(int surveyId, int userId)
        {
            var survey = _db.Surveys.FirstOrDefault(x => x.Id == surveyId && x.UserId == userId);

            if (survey == null)
            {
                return null;
            }

            var dto = new ReviewAnswersDto
            {
                SurveyTitle = survey.Title,
                Answers = new List<ReviewAnswerDto>()
            };

            foreach (var section in survey.Sections)
            {
                foreach (var question in section.Questions)
                {
                    foreach (var answer in question.Answers)
                    {
                        dto.Answers.Add(new ReviewAnswerDto
                        {
                            AnswerText = answer.AnswerText,
                            QuestionId = question.Id,
                            QuestionTitle = question.Title,
                            SectionId = section.Id,
                            SectionTitle = section.Title
                        });
                    }
                }
            }

            return dto;
        }

        public AnswerDto ReadAnswer(int answerId, int userId)
        {
            var answers = _db.Answers
                .SingleOrDefault(x => x.Id == answerId && x.Question.Section.Survey.UserId == userId);

            return Mapper.Map<Answer, AnswerDto>(answers);
        }
    }
}
