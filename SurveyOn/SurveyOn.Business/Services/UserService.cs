﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNet.Identity;
using SurveyOn.Business.Interfaces;
using SurveyOn.Business.Managers;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Services
{
    public class UserService : IUserService
    {

        private readonly SurveyContext _db = new SurveyContext();
        private readonly AppUserManager _userManager = new AppUserManager();

        public UserDto GetUser(string email)
        {
            var user = _db.Users.SingleOrDefault(x => x.Email == email);

            if (user == null)
            {
                return null;
            }

            var userDto = Mapper.Map<User, UserDto>(user);

            return userDto;
        }
        public async Task<IdentityResult> RegisterUser(UserDto user)
        {
            return await _userManager.CreateAsync(user.Email, user.Password).ConfigureAwait(false);
        }

        public async Task<User> FindUser(string email, string password)
        {
            return await _userManager.FindAsync(email, password).ConfigureAwait(false);
        }
    }
}
