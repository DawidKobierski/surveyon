﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using AutoMapper;
using SurveyOn.Business.Interfaces;
using SurveyOn.Data.Models;
using SurveyOn.Dto;

namespace SurveyOn.Business.Services
{
    public class SurveyService : ISurveyService
    {
        private readonly SurveyContext _db = new SurveyContext();

        public IEnumerable<SurveyDto> ReadSurveys(int userId)
        {
            var surveys = _db.Surveys
                .Where(x => x.UserId == userId)
                .ToList()
                .Select(Mapper.Map<Survey, SurveyDto>);

            return surveys;
        }

        public SurveyDto ReadSurvey(int surveyId, int userId = 0)
        {
            var survey = _db.Surveys.SingleOrDefault(x => x.Id == surveyId);

            if (survey == null || (userId != 0 && survey.UserId != userId) 
                || (userId == 0 && survey.IsPublished == false))
            {
                return null;
            }

            var surveyDto = Mapper.Map<Survey, SurveyDto>(survey);

            return surveyDto;
        }
        public IEnumerable<DetailedSurveyDto> ReadDetailedSurveys(int userId)
        {

            var surveys = _db.Surveys
                .Where(x => x.UserId == userId)
                .ToList()
                .Select(Mapper.Map<Survey, DetailedSurveyDto>);

            return surveys;
        }
        public SurveyDto CreateSurvey(int userId)
        {
            var survey = new Survey {UserId = userId};
            _db.Surveys.Add(survey);
            _db.SaveChanges();
            return Mapper.Map<Survey, SurveyDto>(survey);
        }

        public bool UpdateSurvey(SurveyDto surveyDto, int userId)
        {
            var survey = _db.Surveys.SingleOrDefault(x => x.Id == surveyDto.Id && x.UserId == userId);
            if (survey == null)
            {
                return false;
            }

            var surveyUpdate = Mapper.Map<SurveyDto, Survey>(surveyDto);
            var surveyProperties = typeof(Survey).GetProperties();
            foreach (PropertyInfo prop in surveyProperties)
            {

                if (prop.GetValue(surveyUpdate) != null
                    && prop.Name != nameof(Survey.Id)
                    && prop.Name != nameof(Survey.UserId)
                    && prop.Name != nameof(Survey.Sections))
                {
                    prop.SetValue(survey, prop.GetValue(surveyUpdate));
                }
            }

            _db.SaveChanges();

            return true;
        }

        public bool DeleteSurvey(int surveyId, int userId)
        {
            var survey = _db.Surveys.SingleOrDefault(x => x.Id == surveyId && x.UserId == userId);
            if (survey != null)
            {
                _db.Surveys.Remove(survey);
                _db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
