﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyOn.Data.Models
{
    public class Answer : ModelBase
    {
        public string AnswerText { get; set; }

        public string AnswererIp { get; set; }

        public int QuestionId { get; set; }

        [ForeignKey("QuestionId")]
        public virtual Question Question { get; set; }
    }
}
