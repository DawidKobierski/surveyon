﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyOn.Data.Models
{
    public class Survey : ModelBase
    {
        [StringLength(128, ErrorMessage = "Title must be shorter than 128 characters.")]
        public string Title { get; set; }
        [StringLength(512, ErrorMessage = "Description must be shorter than 512 characters.")]
        public string Description { get; set; }
        public bool IsPublished { get; set; }
        public DateTime? PublishDate { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public virtual List<Section> Sections { get; set; }
    }
}
