﻿using System.Data.Entity;

namespace SurveyOn.Data.Models
{
    public class SurveyContext : DbContext
    {
        public DbSet<User> Users{ get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
    }
}
