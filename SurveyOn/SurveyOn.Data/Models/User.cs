﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyOn.Data.Models
{
    public class User : ModelBase
    {
        [StringLength(45)]
        [Index(IsUnique = true)]
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public virtual List<Survey> Surveys { get; set; }
    }
}
