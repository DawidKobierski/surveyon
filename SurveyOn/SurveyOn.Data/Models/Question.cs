﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyOn.Data.Models
{
    public class Question : ModelBase
    {
        [StringLength(128, ErrorMessage = "Title must be shorter than 128 characters.")]
        public string Title { get; set; }      

        [StringLength(512, ErrorMessage = "Description must be shorter than 512 characters.")]
        public string Description { get; set; }
        [Required]
        public bool Required { get; set; }

        public int SectionId { get; set; }

        [ForeignKey("SectionId")]
        public virtual Section Section { get; set; }

        public int QuestionTypeId { get; set; }

        [ForeignKey("QuestionTypeId")]
        public virtual QuestionType QuestionType { get; set; }

        public virtual List<Answer> Answers { get; set; }
    }
}
