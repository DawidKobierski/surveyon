﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SurveyOn.Data.Models
{
    public class Section : ModelBase
    {
        [StringLength(128, ErrorMessage = "Title must be shorter than 128 characters.")]
        public string Title { get; set; }

        [StringLength(512, ErrorMessage = "Description must be shorter than 512 characters.")]
        public string Description { get; set; }

        public int SurveyId { get; set; }

        [ForeignKey("SurveyId")]
        public virtual Survey Survey { get; set; }

        public virtual List<Question> Questions { get; set; }
    }
}
