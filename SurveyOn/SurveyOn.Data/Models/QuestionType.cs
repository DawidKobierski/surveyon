﻿using System.ComponentModel.DataAnnotations;

namespace SurveyOn.Data.Models
{
    public class QuestionType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
