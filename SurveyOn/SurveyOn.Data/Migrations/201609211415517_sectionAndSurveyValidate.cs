namespace SurveyOn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sectionAndSurveyValidate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Sections", "Title", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Sections", "Description", c => c.String(maxLength: 512));
            AlterColumn("dbo.Surveys", "Title", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Surveys", "Description", c => c.String(maxLength: 512));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Surveys", "Description", c => c.String());
            AlterColumn("dbo.Surveys", "Title", c => c.String());
            AlterColumn("dbo.Sections", "Description", c => c.String());
            AlterColumn("dbo.Sections", "Title", c => c.String());
        }
    }
}
