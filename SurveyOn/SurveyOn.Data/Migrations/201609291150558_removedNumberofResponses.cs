namespace SurveyOn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedNumberofResponses : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Surveys", "NumberOfResponses");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Surveys", "NumberOfResponses", c => c.Int(nullable: false));
        }
    }
}
