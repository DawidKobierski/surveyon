namespace SurveyOn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questionModelValidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Questions", "Title", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Questions", "Description", c => c.String(maxLength: 512));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Questions", "Description", c => c.String());
            AlterColumn("dbo.Questions", "Title", c => c.String());
        }
    }
}
