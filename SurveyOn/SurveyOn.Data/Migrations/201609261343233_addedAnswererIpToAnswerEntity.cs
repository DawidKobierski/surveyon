namespace SurveyOn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedAnswererIpToAnswerEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "AnswererIp", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Answers", "AnswererIp");
        }
    }
}
