namespace SurveyOn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renamedAnswerColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "AnswerText", c => c.String());
            DropColumn("dbo.Answers", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Answers", "Name", c => c.String());
            DropColumn("dbo.Answers", "AnswerText");
        }
    }
}
