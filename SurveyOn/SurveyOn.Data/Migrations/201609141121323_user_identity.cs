namespace SurveyOn.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class user_identity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "PasswordHash", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "PasswordHash");
        }
    }
}
