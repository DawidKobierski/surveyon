namespace SurveyOn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class allowEmptyTitleFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Questions", "Title", c => c.String(maxLength: 128));
            AlterColumn("dbo.Sections", "Title", c => c.String(maxLength: 128));
            AlterColumn("dbo.Surveys", "Title", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Surveys", "Title", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Sections", "Title", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Questions", "Title", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
