namespace SurveyOn.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class questionRequiredField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "Required", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "Required");
        }
    }
}
