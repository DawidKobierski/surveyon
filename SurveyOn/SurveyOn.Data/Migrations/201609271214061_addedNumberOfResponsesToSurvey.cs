namespace SurveyOn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedNumberOfResponsesToSurvey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Surveys", "NumberOfResponses", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Surveys", "NumberOfResponses");
        }
    }
}
