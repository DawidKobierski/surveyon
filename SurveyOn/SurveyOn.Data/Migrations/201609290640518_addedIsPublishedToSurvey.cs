namespace SurveyOn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedIsPublishedToSurvey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Surveys", "IsPublished", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Surveys", "IsPublished");
        }
    }
}
