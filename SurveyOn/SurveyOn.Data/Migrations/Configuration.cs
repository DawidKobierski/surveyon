using SurveyOn.Data.Models;

namespace SurveyOn.Data.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<SurveyOn.Data.Models.SurveyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SurveyOn.Data.Models.SurveyContext context)
        {
            context.QuestionTypes.AddOrUpdate(
               qt => qt.Name,
               new QuestionType { Name = "short"},
               new QuestionType { Name = "long"}
                );
        }
    }
}
