// <auto-generated />
namespace SurveyOn.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class renamedAnswerColumn : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(renamedAnswerColumn));
        
        string IMigrationMetadata.Id
        {
            get { return "201609231216222_renamedAnswerColumn"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
