namespace SurveyOn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedCreatedDataFromQuestionType : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.QuestionTypes", "CreatedDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.QuestionTypes", "CreatedDate", c => c.DateTime(nullable: false));
        }
    }
}
