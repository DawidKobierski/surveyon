﻿namespace SurveyOn.Dto
{
    public class ReviewAnswerDto : AnswerDto
    {
        public int SectionId { get; set; }
        public string SectionTitle { get; set; }
        public string QuestionTitle { get; set; }
    }
}