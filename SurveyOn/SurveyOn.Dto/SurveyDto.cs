﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SurveyOn.Dto
{
    public class SurveyDto : DtoBase
    {
        [StringLength(128)]
        public string Title { get; set; }
        [StringLength(512)]
        public string IsPublished { get; set; }
        public string Description { get; set; }
        public string PublishedDate { get; set; }
        public IEnumerable<SectionDto> Sections { get; set; }
    }
}
