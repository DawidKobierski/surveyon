﻿namespace SurveyOn.Dto
{
    public class QuestionTypeDto : DtoBase
    {
        public string Name { get; set; }
    }
}