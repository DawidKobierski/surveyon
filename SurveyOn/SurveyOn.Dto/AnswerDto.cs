﻿using System.Collections.Generic;

namespace SurveyOn.Dto
{
    public class AnswerDto
    {
        public string AnswerText{ get; set; }
        public int QuestionId { get; set; }
    }
}