﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SurveyOn.Dto
{
    public class SectionDto : DtoBase
    {
        [StringLength(128)]
        public string Title { get; set; }
        [StringLength(512)]
        public string Description { get; set; }
        public IEnumerable<QuestionDto> Questions { get; set; }
    }
}
