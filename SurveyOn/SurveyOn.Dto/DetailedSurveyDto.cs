﻿namespace SurveyOn.Dto
{
    public class DetailedSurveyDto : SurveyDto
    {
        public int NumberOfSections { get; set; }
        public int NumberOfQuestions { get; set; }
        public int NumberOfResponses { get; set; }
    }
}
