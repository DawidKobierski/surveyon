﻿using System.Collections.Generic;

namespace SurveyOn.Dto
{
    public class ReviewAnswersDto
    {
        public List<ReviewAnswerDto> Answers { get; set; }
        public string SurveyTitle { get; set; }
    }
}